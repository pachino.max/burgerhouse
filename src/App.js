import React,{Component} from 'react';
import Constructor from './components/Burger/Constructor';
import Burger from './components/Burger/Burger';
import 'bootstrap/dist/css/bootstrap.css';


class App extends Component{

  state = {
    currentPrice: 20,
    ingredients: [
      {name: 'Salad', price: 5, count: 0, id: 1},
      {name: 'Cheese', price: 20, count: 0, id: 2},
      {name: 'Bacon', price: 30, count: 0, id: 3},
      {name: 'Meat', price: 50, count: 0, id: 4}
    ]
  }

  increase =(ingrId)=>{
    const index = this.state.ingredients.findIndex(i=>i.id===ingrId);    

    let newPrice = this.state.currentPrice;
    
    newPrice += this.state.ingredients[index].price;
    this.state.ingredients[index].count++;
    this.setState({currentPrice: newPrice});
  }


  decrease =(ingrId)=>{
    const index = this.state.ingredients.findIndex(i=>i.id===ingrId);    

    let newPrice = this.state.currentPrice;
   
    if (this.state.ingredients[index].count > 0) {
      newPrice -= this.state.ingredients[index].price;
      this.state.ingredients[index].count--;
      this.setState({currentPrice: newPrice})
    }    
  }

  getIngrCount = (ingrId) => {
    const index = this.state.ingredients.findIndex(i => i.id === ingrId);
    const ingredients = [...this.state.ingredients];

    return ingredients[index].count;
  }

  

  render(){
    return(
      <div className="Container">
          <Constructor 
            saladsCount={this.getIngrCount(1)} 
            cheesesCount={this.getIngrCount(2)}
            baconsCount={this.getIngrCount(3)}
            meatsCount={this.getIngrCount(4)}
          />
          <div className="col">
            <p>Current Price: {this.state.currentPrice} som</p>
            <Burger ingredients={this.state.ingredients} increase={this.increase} decrease={this.decrease}/>
            
          </div>       
      </div>            
    )
  }  
}

export default App;
