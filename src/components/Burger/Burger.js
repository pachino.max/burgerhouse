import React from 'react';
import Ingredient from './Ingredient';

const Burger = (props) =>{
    return props.ingredients.map((ingr)=>{
        return (
            <Ingredient 
            key={ingr.id}
            count={ingr.count}
            name={ingr.name}
            price={ingr.price}
            increase={() => props.increase(ingr.id)}
            decrease={() => props.decrease(ingr.id)}
            />
        )
    })
}

export default Burger;