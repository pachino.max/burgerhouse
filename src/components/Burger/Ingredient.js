import React from 'react';
import ToggleButton from '../ToggleButton/ToggleButton';

const Ingredient = (props) =>{
    return(
        <div>
            <p>{props.name}</p>
            <ToggleButton  buttonType='Less' ingrCount={props.count} buttonClick={props.decrease}/>
            <ToggleButton  buttonType='More' ingrCount={props.count} buttonClick={props.increase}/>
        </div>
    )
}

export default Ingredient;