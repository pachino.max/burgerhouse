import React from 'react';
import './Burger.css';

const Constructor = (props) =>{

    let salads = [];
    for (let i = 0; i < props.saladsCount; i++) {
        salads.push(<div class="Salad" ></div>)
      }
      
    
    let cheeses = [];
    for (let i = 0; i < props.cheesesCount; i++) {
        cheeses.push(<div class="Cheese" ></div>)
      }
      let bacons = [];
      for (let i = 0; i < props.baconsCount; i++) {
          bacons.push(<div class="Bacon" ></div>)
        }
        let meats = [];
        for (let i = 0; i < props.meatsCount; i++) {
            meats.push(<div class="Meat" ></div>)
          }
  

    return (
        <div class="Burger">
            <div class="BreadTop">
                <div class="Seeds1"></div>
                <div class="Seeds2"></div>
            </div>
            {salads}
            {cheeses}
            {bacons}
            {meats}
        <div class="BreadBottom"></div>
        </div>
    )

}

export default Constructor;