import React from 'react';
import './ToggleButton.css';

const ToggleButton = (props) =>{

    let buttonClasses = ['ToggleButton'];
    buttonClasses.push('col-md-1 col-sm-2')

    if (props.buttonType == 'More') {
        buttonClasses.push('More');    }

    else if(props.ingrCount == 0){
        buttonClasses.push('InActive');
    }    
    else if (props.buttonType == 'Less') {
        buttonClasses.push('Less');
    }

    return(
            <button className={buttonClasses.join(' ')}
                onClick={props.buttonClick}                
            >
                {props.buttonType}
            </button>
    )
}

export default ToggleButton;